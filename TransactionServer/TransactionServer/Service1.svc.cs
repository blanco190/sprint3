﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
//needed for SQL access
using System.Data.SqlClient;
using System.Collections;

namespace TransactionServer
{
    public class Service1 : IStockExchange
    {
        private String connStr = "Data Source=localhost;Initial Catalog=TransactionServ;Integrated Security=True";

        #region MILESTONE 3 (03/29/2014-04/13/2014)
       

        public RESULT setSystemStartTime(DateTime target_date, String super_password)
        {
            RESULT res = new RESULT();
            //1. try to establish connection, and begin the transaction
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //1. check the super password, if incorrect, throw exception
               

                //2. update the entry on START_TIME
                //e.g., UPDATE TBL_CONFIG SET VALUE = @start_time  WHERE KEY_NAME ='START_DATE'", conn, transaction
               

                //3. update the entry on ACTUAL_START_TIME
                

                transaction.Commit();
            }
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.Message.ToString();
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }

            return res;

        }

        public DateTime getSystemTime()
        {
            DateTime timestamp = DateTime.Now;
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //1. get the START_DATE and ACTUAL_START_DATE and get their difference
                
                //2. get the current time stamp using DateTime.Now, and use the "difference" above to calculate the
                //logical system time of the stock exchange server. 
                
                transaction.Commit();
            }
            catch (Exception exc)
            {
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }

            return new DateTime(0, 1, 1); //THIS SHOULD BE LATER CHANGED TO THE RESULT OF YOUR CALCULATION
        }

        public RESULT_PRICE[] getPriceHistory(String stock_id, int num_records, DateTime lastTimeStamp)
        {
            RESULT_PRICE[] result = null; //will later be instantiated depending on the size of the result.
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //0. if num_records > 1000 reject the request

                //1. retrieve the latest price stock record (ie. largets timestamp which is <= target_date)
                // NOTE! the price of a specified stock is stored in a data table DIRECTLY in the name of the stock_id!!!
                // an example of the query looks like below (using TOP, ORDER BY and WHERE clause)                
                //    SELECT TOP 10 price_cents, timestamp FROM GE 
                //    WHERE  timestamp <@target_date
                //    ORDER BY timestamp DESC 



                //2. if no result returned, close connection and throw new exception


                //3. otherwise, read from the DataReader, maybe using a loop. CHECK the implementation of getStockList() for an example.
                //NOTE: you may have to reverse the array (to make it ascend in the order of time stamp);



                //4. if everything is OK, commit and set up the result array (to return later after the finally clause)
                transaction.Commit();
            }
            catch (Exception exc)
            {
                //5. return a RESULT_PRICE[1] with error message set.
                result = new RESULT_PRICE[1];
                result[0] = new RESULT_PRICE();
                result[0].OK = false;
                result[0].MESSAGE = exc.Message;
                result[0].PRICE_CENTS = -1;
                result[0].TIMESTAMP = DateTime.Now;
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }

            return result;

        }

        public RESULT_PRICE getPrice(String stock_id)
        {
            try
            {

                //1. call getSystemTime() and pass the information to getPriceHistory(), just ask for 1 record


                //2. check the gap between the returned timestamp and the target_date, if more than 10 seconds, throw exception


                //3. if everything ok, return the result returned by getPriceHistory

            }
            catch (Exception exc)
            {
                //4. anything wrong, return the exception message


            }
            return null;
        }

        public RESULT_ACCOUNT getAccountInfo(int account_id, String password)
        {
            //0. declare the RESULT_ACCOUNT to return and initialize it
            RESULT_ACCOUNT res = new RESULT_ACCOUNT();
            res.OK = true;
            res.MESSAGE = "OK";
            res.ACCOUNT = new ACCOUNT();

            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //1. read the account information given account_id and password
                // example: SELECT FNAME, LNAME, BALANCE_CENTS, UNAME FROM TBL_ACCOUNT WHERE ID=@id AND PASSWORD=@password
                

                //3. get the list of stocks owned
                

                //4. get the list of transaction records
                // example SQL:     SELECT stock_id, amount, type, price_cents, fee_cents, timestamp 
                //                  FROM TBL_TRANSACTIONS WHERE account_id=@id
                //                  ORDER BY timestamp ASC
                
                transaction.Commit();
            }
            catch (Exception exc)
            {
                //5. if any error, sets the message in the result to return and rollback
                res.OK = false;
                res.MESSAGE = exc.Message;
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }

            return res;
        }

        public String[] getStockList()
        {
            String[] arrStr = null;
             SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //1.read the stocks
                SqlCommand cmd1 = new SqlCommand("SELECT ID FROM TBL_STOCK",
                     conn, transaction);
                SqlDataReader reader = cmd1.ExecuteReader();
                ArrayList arrStocks = new ArrayList();
                while(reader.Read()){
                    String stock_id = reader.GetString(0);
                    arrStocks.Add(stock_id);
                }
                arrStr = (String [])arrStocks.ToArray(typeof(String));
                reader.Close();
                transaction.Commit();
            }
            catch (Exception exc)
            {
                arrStr = null;
            }
            finally
            {
                conn.Close();
            }
            return arrStr;
        }
        #endregion

        
        #region MILESTONE 2 (already done)
        //Task1 (User Story1): Register Account. Check Requirements in IService1.cs
        public RESULT_ACCOUNT RegisterAccount(String uname, String fname, String lname, String password, long initialBalance_100)
        {            
            RESULT_ACCOUNT res = new RESULT_ACCOUNT();            
            //1. try to establish connection, and begin the transaction
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //2. perform SQL selection, check existence of UNAME
                SqlCommand cmdCheckUname = new SqlCommand("SELECT UNAME FROM TBL_ACCOUNT WHERE UNAME=@uname", conn, transaction);
                cmdCheckUname.Parameters.AddWithValue("@uname", uname);
                SqlDataReader reader = cmdCheckUname.ExecuteReader();                
                if (reader.HasRows)
                {
                    reader.Close();
                    throw new Exception("Username " + uname + " already exists!");
                }
                reader.Close();
                
                //3. perform insertion
                SqlCommand cmdInsert = new SqlCommand(@"INSERT INTO TBL_ACCOUNT(FNAME, LNAME, BALANCE_CENTS, UNAME,  PASSWORD) 
                    VALUES ( @fname, @lname, @balance, @uname, @password)", conn, transaction);
                cmdInsert.ExecuteNonQuery();

                SqlCommand cmdmax = new SqlCommand("SELECT MAX(ID) FROM TBL_ACCOUNT", conn, transaction);
                SqlDataReader reader3 = cmdmax.ExecuteReader();
                reader3.Read();
                res.OK = true;
                res.MESSAGE = "OK";
                res.ACCOUNT_ID = reader3.GetInt32(0);
            }
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.ToString();
                res.ACCOUNT_ID = -1;
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }
            
            return res;
        }

        //Task2 (User Story2): Buy. Check Requirements in IService1.cs
        public RESULT_PRICE buy(int account_id, String password, String stock_id, int amount)
        {
            //1. try to establish connection, and begin the transaction
            RESULT_PRICE res = new RESULT_PRICE();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //2. check validity of password, and retrieve the balance of the account
                SqlCommand cmdCheckBalance = new SqlCommand("SELECT BALANCE_CENTS FROM TBL_ACCOUNT WHERE ID=@id AND PASSWORD=@password", conn, transaction);
                cmdCheckBalance.Parameters.AddWithValue("@id", account_id);
                cmdCheckBalance.Parameters.AddWithValue("@password", password);
                SqlDataReader reader = cmdCheckBalance.ExecuteReader();
                long balance = 0;
                if (!reader.HasRows)
                {
                    reader.Close();
                    throw new Exception("Incorrect password!");
                }
                else
                {
                    reader.Read();
                    balance = reader.GetInt64(0);
                }
                reader.Close();

                //3. get the price of the stock
                RESULT_PRICE price = this.getPrice(stock_id);
                if (price.OK == false)
                {
                    throw new Exception(price.MESSAGE);
                }

                //4. update the balance of the account
                long transaction_amount = amount * price.PRICE_CENTS + 200;
                if (transaction_amount  > balance )
                {
                    throw new Exception("You do not have sufficient funds in your account for a transaction of " +
                        transaction_amount + " cents");
                }
                long remaining_amount = balance - transaction_amount;
                SqlCommand cmdUpdateAccount = new SqlCommand("UPDATE TBL_ACCOUNT SET BALANCE_CENTS=@balance WHERE ID=@id", conn, transaction);
                cmdUpdateAccount.Parameters.AddWithValue("@balance", remaining_amount);
                cmdUpdateAccount.Parameters.AddWithValue("@id", account_id);
                int r1 = cmdUpdateAccount.ExecuteNonQuery();
                if (r1 != 1) throw new Exception("error in updating TBL_ACCOUNT to adjust balance!");

                //5. update the ownership table
                SqlCommand cmdCheckOwnership = new SqlCommand("SELECT AMOUNT FROM TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@stock_id", conn, transaction);
                cmdCheckOwnership.Parameters.AddWithValue("@id", account_id);
                cmdCheckOwnership.Parameters.AddWithValue("@stock_id", stock_id);
                SqlDataReader reader2 = cmdCheckOwnership.ExecuteReader();
                int stock_amount = 0;
                if (reader2.HasRows)
                {
                    reader2.Read();
                    stock_amount = reader2.GetInt32(0);
                    reader2.Close();
                    //do the update
                    SqlCommand cmdUpdateOwnership = new SqlCommand("UPDATE TBL_OWNERSHIP SET AMOUNT=@amount WHERE ACCOUNT_ID=@id AND STOCK_ID=@stock_id", conn, transaction);
                    cmdUpdateOwnership.Parameters.AddWithValue("@id", account_id);
                    cmdUpdateOwnership.Parameters.AddWithValue("@stock_id", stock_id);
                    cmdUpdateOwnership.Parameters.AddWithValue("@amount", stock_amount + amount);
                    int res2 = cmdUpdateOwnership.ExecuteNonQuery();
                    if (res2 != 1) throw new Exception("updating ownership table error!");
                }
                else
                {
                    reader2.Close();
                    SqlCommand cmdInsertOwn = new SqlCommand("INSERT INTO TBL_OWNERSHIP VALUES (@id, @stock_id, @amount)", conn, transaction);
                    cmdInsertOwn.Parameters.AddWithValue("@id", account_id);
                    cmdInsertOwn.Parameters.AddWithValue("@stock_id", stock_id);
                    cmdInsertOwn.Parameters.AddWithValue("@amount", amount);
                    int res3 = cmdInsertOwn.ExecuteNonQuery();
                    if (res3 != 1) throw new Exception("inserting into ownership table error!");
                }

                //6. insert into  the transactions table
                SqlCommand cmdInsertTrans = new SqlCommand("INSERT INTO TBL_TRANSACTIONS VALUES (@id, @stock_id, @amount, 'BUY', @price_cents, @fee_cents, @timestamp)",
                    conn, transaction);
                cmdInsertTrans.Parameters.AddWithValue("@id", account_id);
                cmdInsertTrans.Parameters.AddWithValue("@stock_id", stock_id);
                cmdInsertTrans.Parameters.AddWithValue("@amount", amount);
                cmdInsertTrans.Parameters.AddWithValue("@price_cents", price.PRICE_CENTS);
                cmdInsertTrans.Parameters.AddWithValue("@fee_cents", 200);
                cmdInsertTrans.Parameters.AddWithValue("@timestamp", price.TIMESTAMP);
                int res4 = cmdInsertTrans.ExecuteNonQuery();
                if (res4 != 1) throw new Exception("error in inserting into TBL_TRANSACTIONS");

                //7. finish transaction and return
                res.OK = true;
                res.MESSAGE = "BUY transaction completed!";
                res.PRICE_CENTS = price.PRICE_CENTS;
                res.STOCK_ID = stock_id;
                res.TIMESTAMP = price.TIMESTAMP;                
                transaction.Commit();                
            }
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.ToString();
                res.STOCK_ID = stock_id;
                res.PRICE_CENTS = -1;
                res.TIMESTAMP = DateTime.Now;                
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }
            
            return res;
        }

        //Task3 (User Story3): Sell. Check Requirements in IService1.cs
        public RESULT_PRICE sell(int account_id, String password, String stock_id, int amount)
        {
            //1. try to establish connection, and begin the transaction
            RESULT_PRICE res = new RESULT_PRICE();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //2. check validity of password, and retrieve the balance of the account
                SqlCommand cmdCheckBalance = new SqlCommand("SELECT BALANCE_CENTS FROM TBL_ACCOUNT WHERE ID=@id AND PASSWORD=@password", conn, transaction);
                cmdCheckBalance.Parameters.AddWithValue("@id", account_id);
                cmdCheckBalance.Parameters.AddWithValue("@password", password);
                SqlDataReader reader = cmdCheckBalance.ExecuteReader();
                long balance = 0;
                if (!reader.HasRows)
                {
                    reader.Close();
                    throw new Exception("Incorrect password!");
                }
                else
                {
                    reader.Read();
                    balance = reader.GetInt64(0);
                }
                reader.Close();

                //3. get the price of the stock
                RESULT_PRICE price = this.getPrice(stock_id);
                if (price.OK == false)
                {
                    throw new Exception(price.MESSAGE);
                }

                //4. check/update the ownership table
                SqlCommand cmdCheckOwnership = new SqlCommand("SELECT AMOUNT FROM TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@stock_id", conn, transaction);
                cmdCheckOwnership.Parameters.AddWithValue("@id", account_id);
                cmdCheckOwnership.Parameters.AddWithValue("@stock_id", stock_id);
                SqlDataReader reader2 = cmdCheckOwnership.ExecuteReader();
                int stock_amount = 0;
                if (reader2.HasRows)
                {
                    reader2.Read();
                    stock_amount = reader2.GetInt32(0);
                    reader2.Close();
                    if (stock_amount < amount)
                    {
                        throw new Exception("The account own only " + stock_amount + " of " + stock_id);
                    }
                    //do the update
                    int new_amount = stock_amount - amount;
                    if (new_amount == 0)
                    {
                        SqlCommand cmdDelete = new SqlCommand("DELETE TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@stock_id", conn, transaction);
                        cmdDelete.Parameters.AddWithValue("@id", account_id);
                        cmdDelete.Parameters.AddWithValue("@stock_id", stock_id);
                        int r2 = cmdDelete.ExecuteNonQuery();
                        if (r2 != 1) throw new Exception("DELETE operation error in TBL_OWNERSHIP");
                    }
                    else
                    {
                        SqlCommand cmdUpdateOwnership = new SqlCommand("UPDATE TBL_OWNERSHIP SET AMOUNT=@amount WHERE ACCOUNT_ID=@id AND STOCK_ID=@stock_id", conn, transaction);
                        cmdUpdateOwnership.Parameters.AddWithValue("@id", account_id);
                        cmdUpdateOwnership.Parameters.AddWithValue("@stock_id", stock_id);
                        cmdUpdateOwnership.Parameters.AddWithValue("@amount", stock_amount - amount);
                        int res2 = cmdUpdateOwnership.ExecuteNonQuery();
                        if (res2 != 1) throw new Exception("updating ownership table error!");
                    }
                }
                else
                {
                    reader2.Close();
                    throw new Exception("The account does not own any " + stock_id + "!");
                }

                //5. update the balance of the account
                long transaction_amount = amount * price.PRICE_CENTS - 200;
                long remaining_amount = balance + transaction_amount;
                SqlCommand cmdUpdateAccount = new SqlCommand("UPDATE TBL_ACCOUNT SET BALANCE_CENTS=@balance WHERE ID=@id", conn, transaction);
                cmdUpdateAccount.Parameters.AddWithValue("@balance", remaining_amount);
                cmdUpdateAccount.Parameters.AddWithValue("@id", account_id);
                int r1 = cmdUpdateAccount.ExecuteNonQuery();
                if (r1 != 1) throw new Exception("error in updating TBL_ACCOUNT to adjust balance!");

                
                //6. insert into  the transactions table
                SqlCommand cmdInsertTrans = new SqlCommand("INSERT INTO TBL_TRANSACTIONS VALUES (@id, @stock_id, @amount, 'SELL', @price_cents, @fee_cents, @timestamp)",
                    conn, transaction);
                cmdInsertTrans.Parameters.AddWithValue("@id", account_id);
                cmdInsertTrans.Parameters.AddWithValue("@stock_id", stock_id);
                cmdInsertTrans.Parameters.AddWithValue("@amount", amount);
                cmdInsertTrans.Parameters.AddWithValue("@price_cents", price.PRICE_CENTS);
                cmdInsertTrans.Parameters.AddWithValue("@fee_cents", 200);
                cmdInsertTrans.Parameters.AddWithValue("@timestamp", price.TIMESTAMP);
                int res4 = cmdInsertTrans.ExecuteNonQuery();
                if (res4 != 1) throw new Exception("error in inserting into TBL_TRANSACTIONS");

                //7. finish transaction and return
                res.OK = true;
                res.MESSAGE = "SELL  transaction completed!";
                res.PRICE_CENTS = price.PRICE_CENTS;
                res.STOCK_ID = stock_id;
                res.TIMESTAMP = price.TIMESTAMP;
                transaction.Commit();
            }
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.ToString();
                res.STOCK_ID = stock_id;
                res.PRICE_CENTS = -1;
                res.TIMESTAMP = DateTime.Now;

                transaction.Rollback();
            }
            finally
            {
                conn.Close();
            }

            return res;
        }
        #endregion

    }

 
}
