﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TransactionServer
{
    /// <summary>
    /// IStockExchange defines the SERVICE CONTRACT of the Exchange Server
    /// </summary>
    [ServiceContract]
    public interface IStockExchange
    {

        #region MILESTONE 2 
        /// <summary>
        /// Register an account with the Stock Exchange Server. Deposit and set an initial (i.e.,
        /// amount of cash) of the account
        /// </summary>
        /// <param name="uname">User name of the new account</param>
        /// <param name="fname">First name of the user </param>
        /// <param name="lname">Last name of the user</param>
        /// <param name="password">Password used for identity verification of other operations such as buy/sell</param>
        /// <param name="intialBalance_100">Initial cash deposit in CENTS. E.g., to deposit 100 dollars,
        ///         you need to pass 10000</param>
        /// <returns>If successful, OK attribute will be set to true, and the ACCOUNT_ID sets to the new ID of the account;
        /// if failed, the MESSAGE attribute contains the detailed error message</returns>
        [OperationContract]
        RESULT_ACCOUNT RegisterAccount(String uname, String fname, String lname, String password, long intialBalance_100);

        /// <summary>
        /// Purchase stock_id for account_id. This operation needs the password of the account_id. If successful,
        /// the operation updates the BALANCE_CENTS of the account table, it updates or inserts new records into
        /// ownership table (depending on if the user has already owned the stock). It also inserts a record to the
        /// transaction table to record this transaction.
        /// 
        /// The operation charges a FLAT TRANSACTION FEE of 2 dollars (200 cents)
        /// </summary>
        /// <param name="account_id">The account that wishes to buy.</param>
        /// <param name="password">The password of the account</param>
        /// <param name="stock_id">The stock to buy</param>
        /// <param name="amount">The quantity to buy.</param>
        /// <returns>If the operation is successful, the RESULT_PRICE contains the purchrase_price (and the transaction
        /// timestamp). If failure, the MESSAGE attribute contains the error message. Failure of a purchase operation
        /// can be caused by: (1) incorrect password, (2) market closed - when getPrice() does not return a valid price, 
        /// (3) the account does not have sufficient fund. </returns>
        [OperationContract]
        RESULT_PRICE buy(int account_id, String password, String stock_id, int amount);

        /// <summary>
        /// Sell stock_id for account_id. This operation needs the password of the account_id. If successful,
        /// the operation updates the BALANCE_CENTS of the account table, it updates or deletes  records in the
        /// ownership table (depending on if the user now own any stock of "stock_id"). It also inserts a record to the
        /// transaction table to record this transaction.
        /// 
        /// The operation charges a FLAT TRANSACTION FEE of 2 dollars (200 cents)
        /// </summary>
        /// <param name="account_id">The account that wishes to sell.</param>
        /// <param name="password">The password of the account</param>
        /// <param name="stock_id">The stock to sell</param>
        /// <param name="amount">The quantity to sell.</param>
        /// <returns>If the operation is successful, the RESULT_PRICE contains the sell_price (and the transaction
        /// timestamp). If failure, the MESSAGE attribute contains the error message. Failure in the sell operation
        /// can be caused by: (1) incorrect password, (2) market closed - when getPrice() does not return a valid price, 
        /// (3) the account does not have the quantity of the stock to sell. </returns>
        [OperationContract]
        RESULT_PRICE sell(int account_id, String password, String stock_id, int amount);

        /// <summary>
        /// Get the list of stocks available in the system
        /// </summary>
        /// <returns>return null if error</returns>
        [OperationContract]
        String[] getStockList();
        #endregion //MILESTONE 2


        #region MILESTONE 3 [04/07-04/20/2014]
        /// <summary>
        /// Set the system start date. Will modify the TBL_CONFIG table. This operation needs super_password 
        /// defined in the TBL_CONFIG (if super_password submitted is incorrect, it should reject the request). 
        /// LOGIC: If the request is submitted on "9AM 03/25/2014", and the
        /// specified target_date is "9AM 01/01/1991", then the "START_DATE" and the "ACTUAL_START_DATE"
        /// in TBL_CONFIG will be updated correspondingly to "9AM 01/01/1991" and "9AM 03/25/2014"
        /// correspondingly. Later, these two dates are used to calculate the "current system time". For example,
        /// if a price quote is submitted on "9AM 03/26/2014", then the mapped system time is
        /// "9AM 01/02/1991" (based on the information stored in TBL_CONFIG).
        /// </summary>
        /// <param name="target_date">the specified target date</param>
        /// <param name="super_password">defined in TBL_CONFIG</param>
        /// <returns></returns>
        [OperationContract]
        RESULT setSystemStartTime(DateTime target_date, String super_password);

        /// <summary>
        /// Return the current system time (in its simulated time, e.g., 01/01/1990) - [i.e., not the REAL ACTUAL TIME]
        /// </summary>
        /// <returns>the "simulated" timestamp corresponds to the given date</returns>
        [OperationContract]
        DateTime getSystemTime();

        /// <summary>
        /// Get a history (array) of price information of a given stock ID, sorted in ascending order in date.
        /// All records have timestamp smaller than or equal to the given "lastTimeStamp". The size of the returned array should be "num_records" (as long as
        /// there are data available). Sample usage: "retrieve the last 1000 records of GE price before 01/01/1992 5:00pm". 
        /// *** NOTE 1: the num_records value should NOT exceed 1000! **********
        /// *** NOTE 2: the returned price information should be sorted by timestamp in ASCENDING ORDER!
        /// *** NOTE 3: For each existing stock, there is a DATA TABLE. E.g., table GE for the price records of GE.
        /// </summary>
        /// <param name="stock_id">the stock id</param>
        /// <param name="num_records">the expected size of the returned array</param>
        /// <param name="lastTimeStamp">all history records should have timestamp smaller than or equal to lastTimeStamp. The lastTimeStamp is the "logical 
        /// time" (the simulated history time) [e.g., 01/01/1990] (i.e., not the actual time stamp) </param>
        /// <returns>if successful, returns the array of RESULT_PRICE. If fail, return an array with ONE record with the error message</returns>
        [OperationContract]
        RESULT_PRICE[] getPriceHistory(String stock_id, int num_records, DateTime lastTimeStamp);

        /// <summary>
        /// Get the price of the given stock ID.
        /// If there is not an exact match of the timestamp, the service tries to retrieve the latest match (i.e., the largest timestamp
        /// which is smaller than the request time). If the difference of the returned timestamp (from the requested timestamp)
        /// is greater than the data frequence 10 seconds, the service
        /// returns an ERROR message (MARKET CLOSED). For example, if 01/05/1990 5PM is the last record of the week, and if the request asks for
        /// the price information of 01/06/1990 9AM (Saturday, when the market is closed), then the RESULT_PRICE.OK should be set
        /// to false and a proper error message should be included in the MESSAGE attribute.
        /// 
        /// The operation can be implemeted by calling getPriceHistory() [plus a small logic of checking market closed]
        /// </summary>
        /// <param name="stock_id">stock id (such as "INTL")</param>
        /// <returns>instance of RESULT_PRICE. Failure causes can include: stock_id not exists, or market closed etc.</returns>
        [OperationContract]
        RESULT_PRICE getPrice(String stock_id);

        

        /// <summary>
        /// Retrieve the account information given the account_id, the password of the account is needed. If successful,
        /// the ACCOUNT attribute of the RESULT_ACCOUNT is set properly, it contains the following information:
        /// (1) the account identity (user name etc.)
        /// (2) a list of stocks that are owned by the account
        /// (3) a list of transaction records that are sorted in the ascending order of timestamp
        /// PLEASE READ RESULT_ACCOUNT, ACCOUNT, TRANSACTION_RECORD and OWNERSHIP_RECORD definitions!!! (defined in the bottom of this file)
        /// </summary>
        /// <param name="account_id">an account ID</param>
        /// <param name="password">the password of the account</param>
        /// <returns>the OK attribute of the result is set to TRUE if operation is successful; otherwise see
        /// error message in the MESSAGE attribute. If successful, the ACCOUNT attribute will be properly filled </returns>
        [OperationContract]
        RESULT_ACCOUNT getAccountInfo(int account_id, String password);
        #endregion
    }
    
    [DataContract]
    public class RESULT
    {
        //******** public data below ******************
        [DataMember]
        public bool OK
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string MESSAGE
        {
            get { return stringValue; }
            set { stringValue = value; }
        }     

        // ******** PRIVATE DATE BELOW ****************
        bool boolValue = true;
        string stringValue = "N/A";

    }

    /// <summary>
    /// Entails information of the result of registration/log-in. 
    /// Two cases:
    /// (1) success: OK is true, ACCOUNT_ID has a valid account number
    /// (2) failure: OK is false. MESSAGE contains the detailed error message. ACCOUNT_ID is set to -1.
    /// </summary>
    [DataContract]
    public class RESULT_ACCOUNT
    {
        //******** public data below ******************
        [DataMember]
        public bool OK
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string MESSAGE
        {
            get { return stringValue; }
            set { stringValue = value; }
        }

       

        [DataMember]
        public int ACCOUNT_ID
        {
            get { return account_id; }
            set { account_id = value; }
        }

        [DataMember]
        public ACCOUNT ACCOUNT
        {
            get { return account; }
            set { account = value; }
        }


                     
        // ******** PRIVATE DATE BELOW ****************
        bool boolValue = true;
        string stringValue = "N/A";
        ACCOUNT account;
        int account_id;
    }


    /// <summary>
    /// PRICE_RESULT represents a price quote. OK/MESSAGE contains the query results (success or failure)
    /// STOCK_ID: ID of the stock (e.g., INTL)
    /// PRICE_CENTS: price in cents (must be an integer)
    /// TIMESTAMP: the timestamp of the price quote
    /// </summary>
    [DataContract]
    public class RESULT_PRICE
    {
        //******** public data below ******************
        [DataMember]
        public bool OK
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string MESSAGE
        {
            get { return stringValue; }
            set { stringValue = value; }
        }

        [DataMember]
        public string STOCK_ID
        {
            get { return stock_id; }
            set { stock_id = value; }
        }

        [DataMember]
        public int  PRICE_CENTS
        {
            get { return price_cents; }
            set { price_cents = value; }
        }

        [DataMember]
        public DateTime TIMESTAMP
        {
            get { return time_stamp; }
            set { time_stamp = value; }
        }


        // ******** PRIVATE DATE BELOW ****************
        bool boolValue = true;
        string stringValue = "N/A";
        string stock_id = "UNKNOWN";
        int price_cents = 0;
        DateTime time_stamp;
    }

    /// <summary>
    /// RESULT has two attributes: OK (means if the operation is successful), MESSAGE (contains error message)
    /// </summary>
    [DataContract]
    public class ACCOUNT
    {
        //******** public data below ******************
        [DataMember]
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public String FNAME
        {
            get { return fname; }
            set { fname = value; }
        }

        [DataMember]
        public String LNAME
        {
            get { return lname; }
            set { lname = value; }
        }

        [DataMember]
        public String UNAME
        {
            get { return uname; }
            set { uname = value; }
        }

        [DataMember]
        public String PASSWORD
        {
            get { return password; }
            set { password = value; }
        }

        [DataMember]
        public long BALANCE_100
        {
            get { return balance_100; }
            set { balance_100 = value; }
        }

        [DataMember]
        public  OWNERSHIP_RECORD [] STOCKS
        {
            get { return stocks; }
            set { stocks = value; }
        }

        [DataMember]
        public TRANSACTION_RECORD[] TRANSACTIONS
        {
            get { return transactions; }
            set { transactions = value; }
        }



        // ******** PRIVATE DATE BELOW ****************
        int id;
        String fname;
        String lname;
        String uname;
        String password;
        long balance_100; //the actual balance*100;
        OWNERSHIP_RECORD []  stocks; //the stocks owned by the user
        TRANSACTION_RECORD[] transactions; //list of buy/sell transaction records
        
    }

    /// <summary>
    /// Models the owernship record
    /// </summary>
    [DataContract]
    public class OWNERSHIP_RECORD
    {
        //******** public data below ******************
        [DataMember]
        public String STOCK_ID
        {
            get { return stock_id; }
            set { stock_id = value; }
        }

        [DataMember]
        public int AMOUNT
        {
            get { return amount; }
            set { amount = value; }
        }

        //***** protected data members
        String stock_id;
        int amount;
    }

    [DataContract]
    public class TRANSACTION_RECORD
    {
        //******** public data below ******************
        [DataMember]
        public String STOCK_ID
        {
            get { return stock_id; }
            set { stock_id = value; }
        }

        [DataMember]
        public int AMOUNT
        {
            get { return amount; }
            set { amount = value; }
        }

        [DataMember]
        public String TYPE
        {
            get { return type; }
            set { type = value; }
        }

        [DataMember]
        public int PRICE_CENTS
        {
            get { return price_cents; }
            set { price_cents = value; }
        }

        [DataMember]
        public int FEE_CENTS
        {
            get { return fee_cents; }
            set { fee_cents = value; }
        }

        [DataMember]
        public DateTime TIMESTAMP
        {
            get { return timestamp; }
            set { timestamp = value; }
        }


        //***** protected data members
        String stock_id;
        int amount;
        String type;
        int price_cents;
        int fee_cents;
        DateTime timestamp;
      
    }

}
